            <div class="col-md-12 col-lg-4 sidebar">
              <div class="sidebar-box search-form-wrap">
                <form action="#" class="search-form">
                  <div class="form-group">
                    <span class="icon fa fa-search"></span>
                    <input type="text" class="form-control" id="s" placeholder="Type a keyword and hit enter">
                  </div>
                </form>
              </div>
              <!-- END sidebar-box -->
              <div class="sidebar-box">
                <h3 class="heading">Popular Posts</h3>
                <div class="post-entry-sidebar">
                  <ul>
                    
                  {{-- @forelse($postsSides as $key => $postsSide) --}}
                    <li>
                      <a href="">
                        <img src="{{ asset('assets/wordify/images/img_2.jpg') }}" alt="Image placeholder" class="mr-4">
                        <div class="text">
                          <h4>{{--{{ $postsSide->title }}--}}</h4>
                          <div class="post-meta">
                            <span class="mr-2">{{--{{ $postsSide->publishedAt }} --}}</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    {{-- @empty--}}
                    <li>
                      <a href="">
                        <img src="{{ asset('assets/wordify/images/img_2.jpg') }}" alt="Image placeholder" class="mr-4">
                        <div class="text">
                          <h4>-None-</h4>
                          <div class="post-meta">
                            <span class="mr-2"> - </span>
                          </div>
                        </div>
                      </a>
                    </li>
                    {{-- @endforelse --}}
                  </ul>
                </div>
              </div>
              <!-- END sidebar-box -->

              <div class="sidebar-box">
                <h3 class="heading">Categories</h3>
                <ul class="categories">
                {{--@forelse($categories as $key => $category)--}}
                  <li><a href="#">{{--{{ $category->title }}--}} </a></li>
                  {{--@empty--}}
                  <li><a href="#">None</a></li>
                  {{--@endforelse--}}
                </ul>
              </div>
              <!-- END sidebar-box -->
