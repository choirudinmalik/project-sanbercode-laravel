    <!-- Start section -->
    <section class="site-section pt-5 pb-5">
        <div class="container">
          <div class="row">
            <div class="col-md-12">

              <div class="owl-carousel owl-theme home-slider">
              
             {{--@foreach($postsSides ?? '' as $key => $postsSide)--}}
                <div>
                  <a href="blog-single.html" class="a-block d-flex align-items-center height-lg" style="background-image: url('{{ asset(assets/wordify/images/img_1.jpg) }}'); ">
                    <div class="text half-to-full">
                      <span class="category mb-5">Restaurant</span>
                      <div class="post-meta">
                        
                        <span class="author mr-2"><img src="{{ asset('assets/wordify/images/person_1.jpg') }}" alt="John"> John</span>&bullet;
                        <span class="mr-2">March 15, 2018 </span> &bullet;
                        <span class="ml-2"><span class="fa fa-comments"></span> 3</span>
                        
                      </div>
                      <h3>{{--{{ $postsSide->title }}--}}</h3>
                      <p>{{--{{ $postsSide->summary }}--}}</p>
                    </div>
                  </a>
                </div>
                {{--@endforeach--}}
              </div>
              
            </div>
          </div>
          
        </div>


      </section>
      <!-- END section -->