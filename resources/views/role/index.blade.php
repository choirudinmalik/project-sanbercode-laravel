@extends('admin.app');

@section('content')
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">List role</h3>
      </div>
	   <div class="panel-heading ml-4 mb-2">
                <a href="/role/export" title="Input data"><button name="input" class="btn btn-primary">Download Excel</button></a>
        </div>
	  <div class="panel-heading ml-4 mt-2">
			<a href="/role/create" title="Input role"><button name="input" class="btn btn-primary"> Buat role</button></a>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
			<div class='alert alert-success'>
			{{session('success')}}
			</div>
		@endif
		<table id="role" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Judul</th>
			<th>Isi</th>
            <th>Aksi</th>
          </tr>
          </thead>
          <tbody>
			@forelse($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{ $value -> title}}</td>
				<td>{{ $value -> description}}</td>
				<td style="display:flex;">
					<a href="/role/{{$value -> id}}"><button class="btn btn-primary btn-sm">Show</button> </a>
					<a href="/role/{{$value -> id}}/edit" title="Edit data"><button class="btn btn-primary btn-sm ml-2">Edit</button> </a>
					<form action="/role/{{$value -> id}}" method="POST">
						@csrf
						@method('DELETE')
						<button type="submit" onclick="return confirm('Yakin akan meghapus data ini')" class="btn btn-danger btn-sm ml-2">Hapus</button>
					</form>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="4" align="center"> No Data</td>
			</tr>
			@endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection

@push('script')
<script src="{{asset('/AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#role").DataTable();
  });
</script>
@endpush