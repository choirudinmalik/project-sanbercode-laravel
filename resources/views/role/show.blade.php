@extends('admin.app');

@section('content')
			<div class="panel-heading ml-4 mb-2">
                <a href="/role" title="Input data"><button name="input" class="btn btn-warning">Kembali</button></a>
            </div>
			
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-text-width"></i>
                  Detail role
                </h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <dl>
                  <dt>Judul</dt>
                  <dd>{{$data -> title}}</dd>
                  <dt>Isi</dt>
                  <dd>{{$data -> description}}</dd>
                </dl>
              </div>
              <!-- /.card-body -->
            </div>
@endsection