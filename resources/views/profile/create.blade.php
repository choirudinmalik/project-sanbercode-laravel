@extends('metro.master')
@section('content')
      <section class="site-section py-sm">
        <div class="container">
          							<!-- BEGIN PROFILE CONTENT -->
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title tabbable-line">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                                    </div>
                                                    <ul class="nav nav-tabs">
                                                        <li class="active">
                                                            <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                                        </li>
                                                        
                                                    </ul>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <!-- PERSONAL INFO TAB -->
                                                        <div class="tab-pane active" id="tab_1_1">
                                                            <form role="form" action="/profile" method="POST">
															@csrf
                                                                <div class="form-group">
                                                                    <label class="control-label">First Name</label>
                                                                    <input type="text" id="firstname" name="firstname" value="{{old('firstname', '')}}" placeholder="Tuliskan Nama Depan Anda" class="form-control" /> 
																@error('firstname')
																	<div class="alert alert-danger">{{ $message }}</div>
																@enderror
																</div>
                                                                <div class="form-group">
                                                                    <label class="control-label">Last Name</label>
                                                                    <input type="text" id="lastname" name="lastname" value="{{old('lastname', '')}}" placeholder="Tuliskan Nama Belakang Anda" class="form-control" /> 
																@error('lastname')
																	<div class="alert alert-danger">{{ $message }}</div>
																@enderror
																</div>
                                                                <div class="form-group">
                                                                    <label class="control-label">About</label>
                                                                    <textarea class="form-control" rows="3" id="profile" name="profile" value="{{old('profile', '')}}" placeholder="Jelaskan Secara Singkat Tentang Anda"></textarea>
                                                                @error('profile')
																	<div class="alert alert-danger">{{ $message }}</div>
																@enderror
																</div>
                                                                <div class="margiv-top-10">
                                                                    <button type="submit" class="btn btn-primary">Perbarui Profil</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- END PERSONAL INFO TAB -->
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
            
          </div>
        </div>
      </section>
@endsection