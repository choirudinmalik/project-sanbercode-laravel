@extends('metro.master')
@section('content')
						<!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> User Profile
                            <small></small>
                        </h1>
						<div class="panel-heading ml-4 mb-2">
							<a href="/blog" title="Input data"><button name="input" class="btn btn-primary">Kembali ke blog</button></a>
						</div>
                        <!-- END PAGE TITLE-->
						<div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="profile-sidebar">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet ">
                                        <!-- SIDEBAR USERPIC -->
										<div class="profile-userpic">
                                            <img src="{{asset('assets/image/'.$profil->photo)}}" class="img-responsive" alt="profpic"> </div>
                                        <div class="profile-usertitle">
                                            <h3 class="text-primary"> {{$profil->firstname}} {{$profil->lastname}} </h3>
                                            <h4 class="text-secondary"> Developer </h4>
											<h5> ABOUT: </h5>
											<p>{{$profil->profile}}</p>
                                        </div>
										<div class="profile-usermenu">
                                            <ul class="nav">
                                                <li>
                                                    <a href="/profile/{{$profil -> id}}/edit">
                                                        <i class="icon-settings"></i> Edit Profil </a>
                                                </li>
                                                
                                            </ul>
                                        </div>
									</div>
								</div>
							</div>
						</div>
						
@endsection