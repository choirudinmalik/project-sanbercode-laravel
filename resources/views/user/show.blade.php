@extends('metro.master')
@section('content')
						<!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> User Profile
                            <small></small>
                        </h1>
                        <!-- END PAGE TITLE-->
						<div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PROFILE SIDEBAR -->
                                <div class="profile-sidebar">
                                    <!-- PORTLET MAIN -->
                                    <div class="portlet light profile-sidebar-portlet ">
                                        <!-- SIDEBAR USERPIC -->
										<div class="profile-userpic">
                                            <img src="{{asset('assets/image/'.$profil->photo)}}" class="img-responsive" alt="profpic"> </div>
                                        <div class="profile-usertitle">
                                            <h3 class="text-primary"> {{$profil->firstname}} {{$profil->lastname}} </h3>
                                            <h4 class="text-secondary"> Developer </h4>
											<h5> ABOUT: </h5>
											<p>{{$profil->profile}}</p>
                                        </div>
										
									</div>
								</div>
							</div>
						</div>
						
@endsection