@extends('admin.app');

@section('content')
		<div class="panel-heading ml-4 mb-2">
                <a href="/user" title="Input data"><button name="input" class="btn btn-warning">Kembali</button></a>
        </div>
			
		<div class="ml-4">
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit user</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/user/{{$profil -> id}}" method="POST">
			  @csrf
			  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="status">Status user</label>
                    <select name="status" class="form-control">
                                        <option value=1>Aktif</option>
                                        <option value=0>Tidak Aktif</option>
                    </select>
				  </div>
                  <div class="form-group">
                    <label for="role">Role user</label>
                    <select name="roleId" class="form-control">
                                        <option value=1>Admin</option>
                                        <option value=2>User</option>
                    </select>
					</div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
		</div>
@endsection