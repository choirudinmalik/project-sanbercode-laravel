@extends('admin.app')


@section('content')
          <div class="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Category </h3>
              </div>
                            <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('categories.update',['category'=> $category->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="Title">Title</label>
                    <input type="text" class="form-control" id="title"  name="title" value="{{ old('title' , $category->title) }}" placeholder="Insert Title">
                    @error('judul')
              <div class="alert alert-danger"> {{ $messages }}</div>
              @enderror
                  </div>
                  <div class="form-group">
                    <label for="Content">Content</label>
                    <input type="text" class="form-control" id="content" name="content" value="{{ old('content', $category->content) }}" placeholder="Insert Content">
                    
                    @error('isi')
              <div class="alert alert-danger"> {{ $messages }}</div>
              @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
          </div>
@endsection
