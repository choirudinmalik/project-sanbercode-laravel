@extends('admin.app')


@section('content')
          <div class="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">New Category</h3>
              </div>
                            <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('categories.store') }}" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title"  name="title" value="{{ old('title' , '') }}" placeholder="Insert Title">
                    @error('title')
              <div class="alert alert-danger"> {{ $messages }}</div>
              @enderror
                  </div>
                  <div class="form-group">
                    <label for="content">Content</label>
                    <input type="text" class="form-control" id="content" name="content" value="{{ old('content', '') }}" placeholder="Insert Content">
                    @error('content')
              <div class="alert alert-danger"> {{ $messages }}</div>
              @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
          </div>
@endsection
