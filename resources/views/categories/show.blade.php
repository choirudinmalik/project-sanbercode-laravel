@extends('admin.app')

@section('content')
          <div class="ml-3 mt-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Category {{$category->id}}</h3>
              </div>
                            <!-- /.card-header -->
              <!-- form start -->
               <div class="card-body">
        <a class="btn btn-primary mb-2" href="{{ route('categories.index') }}"> Back</a>
        <table id="detailPertanyaan" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Column</th>
            <th>Value</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>ID</td>
            <td>{{ $category->id ?? '' }}</td>
          </tr>
          <tr>
            <td>Title</td>
            <td>{{ $category->title ?? '' }}</td>
          </tr>
          <tr>
            <td>Content</td>
            <td>{{ $category->content ?? '' }}</td>
          </tr>
          <tr>
            <td>Create At</td>
            <td>{{ $category->created_at ?? '' }}</td>
          </tr>
          <tr>
            <td>Update At</td>
            <td>{{ $category->updated_at ?? '' }}</td>
          </tr>
          </tbody>
        </table>
      </div>
              </div>
          </div>
@endsection
