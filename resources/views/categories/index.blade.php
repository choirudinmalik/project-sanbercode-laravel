@extends('admin.app')


@section('content')
<div class="card">
      <div class="card-header">
        <h3 class="card-title">Categories</h3>
      </div>
	   <div class="panel-heading ml-4 mb-2">
                <a href="/categories/export" title="Input data"><button name="input" class="btn btn-primary">Download Excel</button></a>
        </div>
      <!-- /.card-header -->
      <div class="card-body">
        @if(session('success'))
          <div class="alert alert-success"> {{ session('success') }}</div>
        @endif
        <a class="btn btn-primary mb-2" href="{{ route('categories.create') }}"> Create New Category</a>
        <table id="dataPertanyaan" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Content</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($category as $key => $categ)
          <tr>
            <td>{{ $key +1 }}</td>
            <td>{{ $categ->title ?? '' }}</td>
            <td>{{ $categ->content ?? ''}}</td>
            <td style="display: flex;">
                <a href="{{ route('categories.show', ['category'=> $categ->id]) }}" class="btn btn-info btn-sm">Show</a> <a href="{{ route('categories.edit', ['category'=> $categ->id]) }}" class="btn btn-default btn-sm">Edit</a> <form action="{{ route('categories.destroy', ['category'=> $categ->id]) }}" method="POST">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
          </tr>
          @empty
          <tr>
            <td colspan="7" align="center">No Data</td>
          </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
@endsection
