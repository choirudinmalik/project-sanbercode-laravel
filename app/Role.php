<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = "roles";

    protected $fillable = ['title','description'];
    //
	public $timestamps = false;
	
	//one to one relationship profile
	public function profiles()
    {
        return $this->hasMany('App\Profile', 'roleId');
    }
}
