<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";

    protected $fillable = ['title','content','postId'];
    //
	
	protected $guarded = [];
	
	//one to many relationship (inverse) post
	public function post()
	{
		return $this->belongsTo('App\Post', 'postId');
	}
	
	//like comment relationship
	public function liked_comments()
    {
        return $this->belongsToMany('App\User', 'like_dislike_comments', 'commentId', 'userId');
    }
}
