<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "categories";

    protected $fillable = ['title','content','parentId'];
    //
	
	//many to many relationship post
	public function posts()
    {
        return $this->belongsToMany('App\Post', 'posts_categories', 'categoryId', 'postId');
    }
}
