<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
		
		$profil = User::find(Auth::id())->profile;
		return view('profile.index', compact('profil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$request->validate([
			'firstname' => ['required', 'max:255'],
			'lastname' => ['required', 'max:255'],
			'profile' => ['required']
		]);
		
		$profil = Profile::create([
			'firstname' => $request->firstname,
			'lastname' => $request->lastname,
			'profile' => $request->profile,
			'status' => 1,
			'photo' => 'default.jpg',
			'userId' => Auth::id(),
			'roleId' => 2
		]);
		Alert::success('Berhasil', 'Profil telah ditambahkan');
		return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$profil = Profile::find($id);
		return view('profile.edit', compact('profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$request->validate([
			'firstname' => ['required', 'max:255'],
			'lastname' => ['required', 'max:255'],
			'profile' => ['required']
		]);
		Profile::where('id', $id)
         ->update(['firstname' => $request->firstname,
			'lastname' => $request->lastname,
			'profile' => $request->profile,
			]);
		Alert::success('Berhasil', 'Profil telah diupdate');
		return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}
