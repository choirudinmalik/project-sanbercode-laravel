<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function export() 
    {
        return Excel::download(new RolesExport, 'roles.xlsx');
    }
	
    public function index()
    {
        //
		$data = Role::all();
		return view('role.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
		return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
		$request->validate([
			'judul' => ['required', 'max:255'],
			'isi' => ['required']
		]);
		
		$sql = Role::create([
			'title' => $request->judul,
			'description' => $request->isi,
		]);
		Alert::success('Berhasil', 'Role telah ditambahkan');
		return redirect('/role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
		$data = Role::find($id);

		return view('role.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
		$data = Role::find($id);
		return view('role.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
		$request->validate([
			'judul' => ['required', 'max:255'],
			'isi' => ['required']
		]);
		
		Role::where('id', $id)
         ->update(['title' => $request->judul,
			'description' => $request->isi
			]);
		Alert::success('Berhasil', 'Role telah diupdate');
		return redirect('/role');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
		Role::destroy($id);
		Alert::success('Berhasil', 'Role telah dihapus');
		return redirect('/role');
    }
}
