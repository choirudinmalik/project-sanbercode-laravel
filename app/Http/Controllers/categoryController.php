<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Category;
use Carbon\Carbon;

class categoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
	public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function export() 
    {
        return Excel::download(new CategoryExport, 'category.xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $category = Category::all();
        return view('categories.index',compact('category'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'required',
            'content'   => 'required'
        ]);
        $nows=Carbon::now()->toDateString();

      //  $questions = DB::table('pertanyaan')->insert([
    //       "judul" => $request["judul"],
    //       "isi" => $request["isi"],
    //       "tanggal_dibuat" => $nows,
    //      "tanggal_diperbaharui" => $nows
    //        ]);

        $category = Category::create([
            "title" => $request["title"],
            "content" => $request["content"],
            "parentId" => 1
        ]);        
        return redirect('/categories')->with('success','Save Data Successfull');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $category = Category::find($id);
        return view('categories.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);
        return view('categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $nows=Carbon::now()->toDateString();
        // $questions = DB::table('pertanyaan')->where('id',$id)->update([
        //         "judul" => $request["judul"],
        //         "isi" => $request["isi"],
        //         "tanggal_diperbaharui" => $nows
        //     ]);
        $category = Category::where('id',$id)->update([
                "title" => $request["title"],
                "content" => $request["content"]
        ]);
        return redirect('/categories')->with('success','Update Data Successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Category::destroy($id);
        return redirect('/categories')->with('success','Delete Data Successfull');
    }
}
