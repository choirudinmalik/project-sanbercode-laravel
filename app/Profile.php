<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $table = "profiles";

    protected $guarded = [];
    //
	
	//one to one relationship (inverse) user
	public function user()
    {
        return $this->belongsTo('App\User', 'userId');
    }
	
	//one to many relationship (inverse) role
	public function role()
    {
        return $this->belongsTo('App\Role', 'roleId');
    }
}
