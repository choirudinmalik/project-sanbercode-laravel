<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
	
	//one to one relationship profile
	public function profile()
    {
        return $this->hasOne('App\Profile', 'userId');
    }
	
	//one to many relationship posts
	public function posts()
    {
        return $this->hasMany('App\Post', 'authorId');
    }
	
	//like comment relationship
	public function liked_comments()
    {
        return $this->belongsToMany('App\Comment', 'like_dislike_comments', 'userId', 'commentId');
    }
	
	//like post relationship
	public function liked_posts()
    {
        return $this->belongsToMany('App\Post', 'like_dislike_posts', 'userId', 'postId');
    }
}
