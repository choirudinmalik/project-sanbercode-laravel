<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = "posts";

    protected $fillable = ['title','content','summary','published','publishedAt','authorId'];
    //
	
	//one to many relationship comment
	public function comments()
    {
        return $this->hasMany('App\Comment', 'postId');
    }
	
	//one to many relationship (inverse) post
	public function user()
	{
		return $this->belongsTo('App\User', 'authorId');
	}
	
	//many to many relationship categories
	public function categories()
    {
        return $this->belongsToMany('App\Category', 'posts_categories', 'postId', 'categoryId');
    }
	
	//like post relationship
	public function liked_posts()
    {
        return $this->belongsToMany('App\User', 'like_dislike_posts', 'postId', 'userId');
    }
}
