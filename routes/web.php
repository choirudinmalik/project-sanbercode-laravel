<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('profile.index');
});

//route laravel-excel
Route::get('user/export', 'UserController@export');
Route::get('categories/export', 'categoryController@export');
Route::get('role/export', 'RoleController@export');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/blogAdmin', 'blogAdminController@index');
Route::get('/blog', 'blogUserController@index');

Route::resource('profile', 'ProfileController');

Route::resource('categories','categoryController');

Route::post('comment/post','CommentController@store');
Route::resource('user', 'UserController');
Route::resource('role', 'RoleController');
Route::resource('categories','categoryController');  

