<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFkAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->foreign('authorId')->references('id')->on('users');
        });

        Schema::table('comments', function (Blueprint $table) {
            $table->foreign('postId')->references('id')->on('posts');;
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('parentId')->references('id')->on('categories');
        });

         Schema::table('profiles', function (Blueprint $table) {
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('roleId')->references('id')->on('roles');
        });

         Schema::table('posts_categories', function (Blueprint $table) {
            $table->foreign('postId')->references('id')->on('posts');
            $table->foreign('categoryId')->references('id')->on('categories');
        });

        Schema::table('like_dislike_posts', function (Blueprint $table) {
             $table->foreign('postId')->references('id')->on('posts');
             $table->foreign('userId')->references('id')->on('users');
        });
        Schema::table('like_dislike_comments', function (Blueprint $table) {
            $table->foreign('commentId')->references('id')->on('comments');
            $table->foreign('userId')->references('id')->on('users');
       });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
